-- 메시 하나 출력
Begin("Parent");
InitPos(0, 50, 0);
End();

Begin("Mesh");
InitPos(0,0,0);
InitMeshName("Res\\Object\\ac_4-3.ASE");
InitMaxFrame(30);
InitLoop(0);
InitColor(255,255,255,0);
EventFadeColor(0,255,255,255,200);
EventFadeColor(0.4,255,255,255,255);
EventFadeColor(0.8,255,255,255,200);
End();

