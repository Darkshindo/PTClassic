//		아이템 정보		//


*이름		"플레이티드 소드"
*Name		"Plated Sword"
*코드		"WS210"

///////////////	공통사항		 ////////////

*내구력		60 80
*무게		13
*가격		16000

///////////////	원소		/////////////

*생체		
*대자연		
*불		
*냉기		
*번개		
*독		
*물		
*바람		

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		13 16	21 25
*사정거리	
*공격속도	7
*명중력		50 62
*크리티컬	12

//////////////	방어성능		/////////////
// 추가요인

*흡수력		
*방어력		
*블럭율		7 7

//////////////	신발성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생	
*근력재생	
*생명력추가	
*기력추가	
*근력추가	
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		37
*힘		76
*정신력		
*재능		82
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승	
*기력상승	
*근력상승	

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

**특화 Knight
**특화랜덤 Mechanician Fighter Pikeman
// Pilgrim

**공격력	0	5
**명중력	1	3
**공격속도	1

*연결파일	"name\WS210.zhoon"
