//		아이템 정보		//


*이름		"스파이크 쉴드"
*Name		"Spike Shield"
*코드		"DS112"

///////////////	공통사항		 ////////////

*내구력		85 110
*무게		38
*가격		56000

///////////////	원소		/////////////

*생체		2 4
*불		2 4
*냉기		2 4
*번개		2 4
*독		2 4

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		
*사정거리		
*공격속도		
*명중력		
*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

*흡수력		2.6 3.099
*방어력		76 100
*블럭율		16	18.999

//////////////	신발성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생
*근력재생
*생명력추가
*기력추가		
*근력추가
*마법기술숙련도

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		60
*힘		112
*정신력		
*재능		60
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승
*기력상승		
*근력상승	

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

**특화랜덤 Knight Atalanta Mechanician Fighter Pikeman Archer

//Rogue Pilgrim //

**흡수력	0.6	0.899
**블럭율	2
**방어력	16	20

*연결파일        "name\DS112.zhoon"