//		아이템 정보		//


*이름		"피닉스 아머"
*Name		"PhoeniX Armor"
*코드		"DA123"

///////////////	공통사항		 ////////////

*내구력		180 210
*무게		138
*가격		1100000

///////////////	원소		/////////////

*생체		10 16
*불		10 16
*냉기		10 16
*번개		10 16
*독		10 16

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		
*사정거리		
*공격속도		
*명중력		
*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

*흡수력		23.5	23.899
//*흡수력		20.9	21.499
*방어력		410	430
*블럭율		

//////////////	이동성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생
*근력재생
*생명력추가
*기력추가		
*근력추가
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		100
*힘		208
*정신력		
*재능		90
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승
*기력상승		
*근력상승

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

**특화랜덤 Mechanician Fighter Pikeman Archer Knight Atalanta
// Rogue Pilgrim//

**흡수력	1.4	1.699
**방어력	50	62

*연결파일        "name\da123.zhoon"