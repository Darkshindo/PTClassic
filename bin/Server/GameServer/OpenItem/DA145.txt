//		아이템 정보		//


*이름		"탈레스 아머(7일)"
*Name		"Taless Armor(7Day)"
*코드		"DA145"

///////////////	공통사항		 ////////////

*내구력		150 150
*무게		100
//*가격		220000

///////////////	원소		/////////////

//*생체		5 12
//*불		5 12
//*냉기		5 12
//*번개		5 12
//*독		5 12

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		
*사정거리		
*공격속도		
*명중력		
*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

*흡수력		10.8	10.899
//*흡수력		12.3	12.799
*방어력		379	379
*블럭율		

//////////////	이동성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생
*근력재생
*생명력추가	20
*기력추가	20	
*근력추가
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		50
//*힘		180
*정신력		
//*재능		80
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승
*기력상승		
*근력상승

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

//**특화랜덤 Mechanician Fighter Pikeman Archer Knight Atalanta
// Rogue Pilgrim//

//**흡수력	0.9	1.499
//**방어력	36	50

*연결파일        "name\da145.zhoon"